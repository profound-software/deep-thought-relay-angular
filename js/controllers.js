'use strict';

var dtAppControllers = angular.module('dtControllers',['dtServices']);

/***
 * Nav Controller
 */
dtAppControllers.controller("NavController", function($rootScope, $state, $scope) {

});

dtAppControllers.controller('TwitterController', function($scope, Twitter) {
  // get your timeline
  var refresh = function() {
    Twitter.query(
      {'act':'home_timeline.json','count':10},
      function(response) {
        $scope.tweets = response
      }
    )
  }
  refresh() // run on load

  $scope.refresh = refresh

  $scope.create = function(newTweet) {
    newTweet.act = "update.json"
    Twitter.save(newTweet,function(response) {
      refresh()
    })
  }
})
