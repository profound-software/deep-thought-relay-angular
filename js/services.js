'use strict';

var relay = dt.relay('api',"http://localhost:8887/default.php")
dtApp.factory('DTRelayInterceptor',function($window){
  return {
    request: function(config) {
      config.params = relay.data(config.params)
      return config
    },
    response: function(response){
      if(response.status==278){ // client-side redirect
        $window.location.href=response.data.location
      }
      return response
    }
  }
})
dtApp.config(['$httpProvider',function($httpProvider){
  $httpProvider.interceptors.push('DTRelayInterceptor')
}])

angular.module('dtServices', ['ngResource'])
.factory("Twitter",function($resource) {
  return $resource(relay.url,{'end':'statuses/','api':'twitter'})
})
