Deep Thought Relay Client
=========================

### TwitterService
To create a service that can talk to a DTRelay, you call **dt.relay()**, which establishes a connection and prepares you for requests. You will also want to add a custom interceptor to give the relay an opportunity to preprocess the request and handle the _authorize_url_ redirect (HTTP 278 status code).

> Under the hood, dt.relay() sends an initial (one-time) request to the relay to retrieve session credentials. In a production environment, the relay should **_always_** be initialized over an encrypted connection! Exposure of the session credentials could lead to client session hijacking.

You may create as many Services as you like to use the relay. In this example, we will create a single service to access the _statuses/_ endpoint on the Twitter API. We specify the API as "twitter", which refers to the `twitter` API in _apis.json_, which we will create in the next section.

#### services.js
```javascript
// establish the connection to the relay
var relay = dt.relay('api',"http://localhost:8887/default.php")

// sprinkle in an interceptor
dtApp.factory('DTRelayInterceptor',function($window){
  return {
    request: function(config) {
      config.params = relay.data(config.params)
      return config
    },
    response: function(response){
      if(response.status==278){ // client-side redirect
        $window.location.href=response.data.location
      }
      return response
    }
  }
})

// add a service connecting to the Twitter API endpoint (statuses/)
angular.module('dtServices', ['ngResource'])
.factory("Twitter",function($resource) {
  return $resource(relay.url,{'end':'statuses/','api':'twitter'})
})
```

### TwitterController

To work with the Service, you are going to create a pretty normal-looking Controller. The request parameters will be passed directly to the relevant API. You can optionally specify an action parameter `act`, which will be appended to the URL of the request (e.g. _statuses/update.json_).

#### controllers.js
```javascript
dtAppControllers.controller('TwitterController', function($scope, Twitter) {
  // get your timeline
  var refresh = function() {
    Twitter.query(
      {'act':'home_timeline.json','count':10},
      function(response) {
        $scope.tweets = response
      }
    )
  }
  refresh() // run on load

  $scope.refresh = refresh

  $scope.create = function(newTweet) {
    newTweet.act = "update.json"
    Twitter.save(newTweet,function(response) {
      refresh()
    })
  }
})
```

### The Usual

From here, you can add the controller to your main page via the states in the _app.js_ file.

#### app.js

```javascript
.state('index', {
  url:'/',
  templateUrl: 'partials/index.html',
  parent: 'body',
  controller: 'TwitterController'
})
```

Nothing surprising there or in the index page, which you can craft to your heart's content.

#### index.html
```html
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1>Deep Thought Relay Twitter App</h1>
    </div>
  </div>

  <!-- New Tweet -->
  <div class='row'>
    <div class='col-md-12'>
      <form class="form" ng-submit='create(newTweet)'>
        <fieldset>
          <legend>Status Update</legend>
          <div class="form-group">
            <label for="newTweet">Tweet</label>
            <input type="text" class="form-control"
              id="newTweet" placeholder="create a new post"
              maxlength="180" ng-model='newTweet.status'>
            <p class="help-block">180 characters</p>
          </div>
          <input type="submit" class='btn btn-primary' value="Post">
        </fieldset>
      </form>
    </div>
  </div>

  <!-- Timeline -->
  <div class="row">
    <div class="col-md-12">
      <h2>
        Feed <button ng-click='refresh()'class='btn btn-success btn-xs'>
        <span class="glyphicon glyphicon-refresh"></span></button>
      </h2>
      <div class="row" ng-repeat='tweet in tweets'>
        <div class="col-md-12 well">
          <div class="row">
            <div class="col-xs-3">
              <img src="{{ tweet.user.profile_image_url}}"
                alt="user profile image" class='img-responsive img-rounded' />
            </div>
            <div class="col-xs-9">
              <h3>
                <a href="http://twitter.com/{{tweet.user.screen_name}}" target='_blank'>
                  @{{tweet.user.screen_name}}
                </a>
              </h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <p>
                {{tweet.text}}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
```

